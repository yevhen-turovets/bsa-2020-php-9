<?php

declare(strict_types=1);

namespace App\Actions;

use App\Exceptions\Api\InvalidAttributeException;
use App\Services\Contracts\MarketDataService;
use App\Actions\Requests\GetChartDataRequest;
use App\Actions\Responses\GetChartDataResponse;
use App\Services\Exceptions\InvalidFrequencyException;
use App\Exceptions\Api\LogicException;

class GetChartDataAction
{
	private MarketDataService $marketDataService;

	public function __construct(MarketDataService $marketDataService)
	{
		$this->marketDataService = $marketDataService;
	}

	public function execute(GetChartDataRequest $request): GetChartDataResponse
	{
	    $this->assertDate($request->startDate, $request->endDate);

		try {
			$chartDataCollection = $this->marketDataService->getChartData(
				$request->startDate,
				$request->endDate,
				$request->frequency,
			);

			return new GetChartDataResponse(
				$chartDataCollection
			);
		} catch (InvalidFrequencyException $e) {
			throw new LogicException($e->getMessage());
		}
	}

    private function assertDate(\DateTime $startDate, \DateTime $endDate)
    {
        if ($startDate->format('Y-m-d H:i:s') >= $endDate->format('Y-m-d H:i:s')) {
            throw new InvalidAttributeException('The start date for analytic data must not exceed the end date');
        }
    }
}
