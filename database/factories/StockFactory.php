<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Stock;
use \App\Entities\User;
use Faker\Generator as Faker;

$factory->define(Stock::class, function (Faker $faker) {
    return [
        'user_id' => User::query()->inRandomOrder()->first()->id,
        'price' => floatval($faker->randomFloat()),
        'start_date' => $faker->dateTime()
    ];
});
