<?php

namespace Tests\Feature;

use App\Entities\Stock;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Faker\Factory as Faker;

class MarketApiTest extends TestCase
{
    use RefreshDatabase;

    private User $user;
    private $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Faker::create();

        $this->seedFakeData();

        $this->user = User::query()->first();
    }

    protected function seedFakeData(): void
    {
        factory(User::class, 2)->create();
        factory(Stock::class, 30)->create();
    }

    public function testCreationStockWithAuth()
    {
        $stock = DB::table('stocks')->orderBy('id', 'desc')->first();

        $stockData = $this->createStockData();

        $this->actingAs($this->user)
            ->post('/api/stocks', $stockData)
            ->assertStatus(201)
            ->assertJsonFragment($stockData);

        $this->assertDatabaseHas('stocks', array_merge(['id' => $stock->id+1], $stockData));
    }

    public function testCreationStockWithoutAuth()
    {
        $stockData = $this->createStockData();

        $this->post('/api/stocks', $stockData)
            ->assertStatus(302);

        $this->assertDatabaseMissing('stocks', $stockData);
    }

    public function testDeletionMyStock()
    {
        $stock = DB::table('stocks')->where('user_id', $this->user->id)->first();

        $this->actingAs($this->user)
            ->delete("/api/stocks/$stock->id")
            ->assertStatus(204);

        $this->assertDatabaseMissing('stocks', array_merge(['id' => $stock->id]));
    }

    public function testDeletionStockWithoutAuth()
    {
        $stock = DB::table('stocks')->first();

        $this->delete("/api/stocks/$stock->id")
            ->assertStatus(302);

        $this->assertDatabaseHas('stocks', array_merge(['id' => $stock->id]));
    }

    public function testDeletionNotMyStock()
    {
        $stock = DB::table('stocks')->whereNotIn('user_id', [$this->user->id])->first();

        $this->actingAs($this->user)
            ->delete("/api/stocks/$stock->id")
            ->assertStatus(404)
            ->assertJson(['message' => 'Stock not found']);

        $this->assertDatabaseHas('stocks', array_merge(['id' => $stock->id]));
    }

    public function testCreationStockWithInvalidPrice()
    {
       $stockData = [
           'start_date' => $this->formatDate($this->faker->dateTimeBetween('today', 'now')),
           'price' => -1,
       ];

       $this->actingAs($this->user)
            ->post('/api/stocks', $stockData)
            ->assertStatus(400)
            ->assertJson(['message' => 'Price must be greater than 0']);

        $this->assertDatabaseMissing('stocks', $stockData);
    }

    public function testCreationStockWithInvalidDate()
    {
        $stockData = [
            'start_date' => $this->formatDate($this->faker->dateTimeBetween('-2 day', 'today')),
            'price' => floatval($this->faker->randomFloat()),
        ];

        $this->actingAs($this->user)
            ->post('/api/stocks', $stockData)
            ->assertStatus(400)
            ->assertJson(['message' => 'Promotions cannot be added for the previous date']);

        $this->assertDatabaseMissing('stocks', $stockData);
    }

    public function testGetCharData()
    {
        $data = [
            'start_date' => Carbon::parse(DB::table('stocks')->min('start_date'))->timestamp,
            'end_date' => Carbon::parse(DB::table('stocks')->max('start_date'))->timestamp,
            'frequency' => 60 * 60 * 24,
        ];

       $this->json("GET","/api/chart-data",$data)->assertStatus(200);
    }

    public function testGetCharDataWithInvalidDate()
    {
        $data = [
            'start_date' => Carbon::parse(DB::table('stocks')->max('start_date'))->timestamp,
            'end_date' => Carbon::parse(DB::table('stocks')->min('start_date'))->timestamp,
            'frequency' => 60 * 60 * 24,
        ];

        $this->json("GET","/api/chart-data",$data)
            ->assertStatus(400)
            ->assertJson(['message' => 'The start date for analytic data must not exceed the end date']);
    }

    public function testGetCharDataWithInvalidFrequency()
    {
        $data = [
            'start_date' => Carbon::parse(DB::table('stocks')->min('start_date'))->timestamp,
            'end_date' => Carbon::parse(DB::table('stocks')->max('start_date'))->timestamp,
            'frequency' => -1,
        ];

        $this->json("GET","/api/chart-data",$data)
            ->assertStatus(400)
            ->assertJson(['message' => 'Frequency must be greater than 0',]);
    }

    private function createStockData()
    {
        return [
            'start_date' => $this->formatDate($this->faker->dateTimeBetween('today', 'now')),
            'price' => floatval($this->faker->randomFloat()),
        ];
    }

    private function formatDate($date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
