<?php

namespace Tests\Unit;

use App\Entities\Stock;
use App\Repositories\Contracts\StockRepository;
use App\Services\Exceptions\InvalidFrequencyException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use App\Services\MarketDataService;

class MarketServiceTest extends TestCase {
    private StockRepository $stockRepositoryStub;
    private MarketDataService $marketDataService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->stockRepositoryStub = $this->createMock(StockRepository::class);

        $this->marketDataService = new MarketDataService($this->stockRepositoryStub);
    }

    public function test_getChartData_returns_data_correctly_by_hour()
    {
        $startDate = new \DateTime('2020-06-01 00:00:00');
        $endDate = new \DateTime('2020-06-02 00:00:00');
        $frequency = 60 * 60;

        $this->stockRepositoryStub->method('findByCriteria')->willReturn(
            $this->getHydratedStock($startDate, $endDate)
        );

        $data = $this->marketDataService->getChartData(
            $startDate,
            $endDate,
            $frequency
        );

        $expectedData = [
            [
                'price' => 23.12,
                'start_date' => new \DateTime('2020-06-01 09:00:00'),
            ],
            [
                'price' => 23.98,
                'start_date' => new \DateTime('2020-06-01 10:00:00'),
            ],
            [
                'price' => 20.15,
                'start_date' => new \DateTime('2020-06-01 11:00:00'),
            ],

        ];

        foreach ($data as $key => $value) {
            $this->assertEquals($expectedData[$key]['price'], $value->getPrice());
            $this->assertEquals($expectedData[$key]['start_date'], $value->getDate());
        }

        return true;
    }

    public function test_getChartData_returns_data_correctly_by_day()
    {
        $startDate = new \DateTime('2020-06-01 00:00:00');
        $endDate = new \DateTime('2020-06-08 00:00:00');
        $frequency = 60 * 60 * 24;

        $this->stockRepositoryStub->method('findByCriteria')->willReturn(
            $this->getHydratedStock($startDate, $endDate)
        );

        $data = $this->marketDataService->getChartData(
            $startDate,
            $endDate,
            $frequency
        );

        $expectedData = [
            [
                'price' => 23.69333333333334,
                'start_date' => new \DateTime('2020-06-01 00:00:00'),
            ],
            [
                'price' => 23.12,
                'start_date' => new \DateTime('2020-06-03 00:00:00'),
            ],
        ];

        foreach ($data as $key => $value) {
            $this->assertEquals($expectedData[$key]['price'], $value->getPrice());
            $this->assertEquals($expectedData[$key]['start_date'], $value->getDate());
        }

        return true;
    }

    public function test_getChartData_returns_data_correctly_by_week()
    {
        $startDate = new \DateTime('2020-06-01 00:00:00');
        $endDate = new \DateTime('2020-06-15 00:00:00');
        $frequency = 60 * 60 * 24 * 7;

        $this->stockRepositoryStub->method('findByCriteria')->willReturn(
            $this->getHydratedStock($startDate, $endDate)
        );

        $data = $this->marketDataService->getChartData(
            $startDate,
            $endDate,
            $frequency
        );

        $expectedData = [
            [
                'price' => 23.550000000000004,
                'start_date' => new \DateTime('2020-06-01 00:00:00'),
            ],
            [
                'price' => 7.04,
                'start_date' => new \DateTime('2020-06-08 00:00:00'),
            ],
        ];

        foreach ($data as $key => $value) {
            $this->assertEquals($expectedData[$key]['price'], $value->getPrice());
            $this->assertEquals($expectedData[$key]['start_date'], $value->getDate());
        }

        return true;
    }

    public function test_getChartData_returns_data_empty()
    {
        $startDate = new \DateTime('2020-06-01 00:00:00');
        $endDate = new \DateTime('2020-06-15 00:00:00');
        $frequency = 60 * 60 * 24 * 7;

        $this->stockRepositoryStub->method('findByCriteria')->willReturn(
           new Collection()
        );

        $data = $this->marketDataService->getChartData(
            $startDate,
            $endDate,
            $frequency
        );

        $this->assertEmpty($data);

        return true;
    }

    public function test_getChartData_returns_invalid_frequency_exception()
    {
        $this->expectException(InvalidFrequencyException::class);
        $this->expectExceptionMessage('Frequency must be greater than 0');

        $startDate = new \DateTime('2020-06-01 00:00:00');
        $endDate = new \DateTime('2020-06-15 00:00:00');
        $frequency = 0;

        $this->stockRepositoryStub->method('findByCriteria')->willReturn(
            $this->getHydratedStock($startDate, $endDate)
        );

        $this->marketDataService->getChartData(
            $startDate,
            $endDate,
            $frequency
        );

        return true;
    }

    private function getHydratedStock($startDate, $endDate)
    {
        return $this->getStock()->map(function($arr) {
            return new Stock( $arr );
        })->whereBetween('start_date', [
            $startDate,
            $endDate,
        ])->sortBy('start_date');
    }

    private function getStock(): Collection
    {
        return collect([
                           [
                               'price'      => 23.12,
                               'start_date' => '2020-06-01 09:01:01',
                           ],
                           [
                               'price'      => 19.89,
                               'start_date' => '2020-06-01 10:01:01',
                           ],
                           [
                               'price'      => 28.07,
                               'start_date' => '2020-06-01 10:15:01',
                           ],
                           [
                               'price'      => 23.12,
                               'start_date' => '2020-06-03 09:01:01',
                           ],
                           [
                               'price'      => 07.04,
                               'start_date' => '2020-06-08 10:45:01',
                           ],

        ]);
    }
}
